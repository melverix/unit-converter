package com.mszewczyk.unit_converter;

import com.mszewczyk.converter_app.MetricToImperialConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


class MetricToImperialConverterTest {
    private MetricToImperialConverter metricToImperialConverter;

    @BeforeEach
    void setUp(){
        metricToImperialConverter = new MetricToImperialConverter();
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void metersToFeetWithValueSource(double value){

        //when
        double expectedResult = 3.28 * value;
        double result = metricToImperialConverter.convertMetersToFeet(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroMeters_shouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> metricToImperialConverter.convertMetersToFeet(-1));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void centimetersToInchesWithValueSource(double value){

        //when
        double expectedResult = 0.39 * value;
        double result = metricToImperialConverter.convertCentimetersToInches(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroCentimetersShouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> metricToImperialConverter.convertCentimetersToInches(-1));
    }


    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void literToGallonWithValueSource(double value){

        //when
        double expectedResult = 0.22 * value;
        double result = metricToImperialConverter.convertLitersToGallons(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroLiters_shouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> metricToImperialConverter.convertLitersToGallons(-1));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void zeroKilogram_shouldReturnZero(double value){

        //when
        double expectedResult = 2.20 * value;
        double result = metricToImperialConverter.convertKilogramsToPounds(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroKilograms_shouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> metricToImperialConverter.convertKilogramsToPounds(-1));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, -20, 0.5, 1000})
    void celsiusToFahrenheitDegreeWithValueSource(double value){

        //when
        double expectedResult = (1.8 * value) + 32;
        double result = metricToImperialConverter.convertCelsiusToFahrenheit(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }
}
