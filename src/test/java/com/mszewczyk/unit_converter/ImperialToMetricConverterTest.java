package com.mszewczyk.unit_converter;

import com.mszewczyk.converter_app.ImperialToMetricConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ImperialToMetricConverterTest {
    private ImperialToMetricConverter imperialToMetricConverter;

    @BeforeEach
    void setUp(){
        imperialToMetricConverter = new ImperialToMetricConverter();
    }
    
    @ParameterizedTest
    @ValueSource(doubles = {0, 5, 0.34, 1215})
    void feetToMetersWithValueSource(double value){

        //when
        double expectedResult = 0.30 * value;
        double result = imperialToMetricConverter.convertFeetToMeters(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroFoots_shouldThrowArgumentIllegalException(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> imperialToMetricConverter.convertFeetToMeters(-1));
    }
    
    @ParameterizedTest
    @ValueSource(doubles = {0, 5, 0.34, 1215})
    void testInchesToCentimetersWithValueSource(double value){

        //when
        double expectedResult = 2.54 * value;
        double result = imperialToMetricConverter.convertInchesToCentimeters(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroLime_shouldThrowIllegalArgumentException(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> imperialToMetricConverter.convertInchesToCentimeters(-1));
    }


    @ParameterizedTest
    @ValueSource(doubles = {0, 5, 0.34, 1215})
    void gallonsToLiteresWithValueSource(double value){

        //when
        double expectedResult = 4.54 * value;
        double result = imperialToMetricConverter.convertGallonsToLiters(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroGallon_shouldThrowIllegalArgumentException(){

        //when
        Assertions.assertThrows(IllegalArgumentException.class, () -> imperialToMetricConverter.convertGallonsToLiters(-1));
    }


    @ParameterizedTest
    @ValueSource(doubles = {0, 5, 0.34, 1215})
    void poundToKilogramsWithValueSource(double value){

        //when
        double expectedResult = 0.45 * value;
        double result = imperialToMetricConverter.convertPoundsToKilograms(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThenZeroPound_shouldThrowIllegalArgumentException(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> imperialToMetricConverter.convertGallonsToLiters(-1));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 32, -4, 0.5, 1000})
    void FahrenheitToCelsiusDegreeWithValueSource(double value){

        //when
        double expectedResult = (value - 32) / 1.8;
        double result = imperialToMetricConverter.convertFahrenheitToCelsius(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }
}
