package com.mszewczyk.converter_app;

public class ImperialToMetricConverter {

    private String valueIsLessThanZeroException = "Value entered cannot be less than zero!";

    public double convertFeetToMeters(double feet) {
        final double factor = 0.30;
        if(feet < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * feet;
    }

    public double convertInchesToCentimeters(double inches) {
        final double factor = 2.54;
        if(inches < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * inches;
    }

    public double convertGallonsToLiters(double gallons) {
        final double factor = 4.54;
        if(gallons < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * gallons;
    }

    public double convertPoundsToKilograms(double pounds) {
        final double factor = 0.45;
        if(pounds < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * pounds;
    }

    public double convertFahrenheitToCelsius(double fahrenheitDegrees) {
        final double factor = 1.8;
        return (fahrenheitDegrees - 32) / factor;
    }


}
