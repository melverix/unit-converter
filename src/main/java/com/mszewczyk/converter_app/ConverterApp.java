package com.mszewczyk.converter_app;

import java.util.Scanner;

class ConverterApp {
    private ImperialToMetricConverter imperialToMetricConverter = new ImperialToMetricConverter();
    private MetricToImperialConverter metricToImperialConverter = new MetricToImperialConverter();
    private Scanner scanner = new Scanner(System.in);

    ConverterApp() {
        runConvererApp();
    }

    private void runConvererApp() {
        System.out.println("=====UNIT CONVERTER====");
        boolean running = true;
        while (running) {
            displayMainMenu();
            String choice = scanner.next().toLowerCase();
            switch (choice) {
                case "1":
                    launchToMetricConverter();
                    break;
                case "2":
                    launchToImperialConverter();
                    break;
                case "exit":
                    running = false;
                    break;
                default:
                    System.out.println("Wrong option, try again!");
                    break;
            }
        }
    }

    private void displayMainMenu() {
        System.out.println("Choose option: \n");
        System.out.println("1. Convert metric units to imperial units");
        System.out.println("2. Convert imperial units to metric units");
        System.out.println("Type 'exit' to quit");
    }


    private void launchToMetricConverter() {
        boolean running = true;
        double convertedValue;
        double enteredValue;
        String sourceUnit;
        String targetUnit;
        while (running) {
            displayToMetricConverterOptions();
            String option = scanner.next();
            switch (option) {
                case "1":
                    sourceUnit = Units.METERS.getUnitName();
                    targetUnit = Units.FEET.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = metricToImperialConverter.convertMetersToFeet(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "2":
                    sourceUnit = Units.CENTIMETERS.getUnitName();
                    targetUnit = Units.INCHES.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = metricToImperialConverter.convertCentimetersToInches(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "3":
                    sourceUnit = Units.LITERS.getUnitName();
                    targetUnit = Units.GALLONS.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = metricToImperialConverter.convertLitersToGallons(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "4":
                    sourceUnit = Units.KILOGRAMS.getUnitName();
                    targetUnit = Units.POUNDS.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = metricToImperialConverter.convertKilogramsToPounds(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "5":
                    sourceUnit = Units.DEGREES_CELSIUS.getUnitName();
                    targetUnit = Units.DEGREES_FAHRENHEIT.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = metricToImperialConverter.convertCelsiusToFahrenheit(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "6":
                    running = false;
                    break;
                default:
                    System.out.println("Wrong option, try again!");
                    break;
            }
        }

    }

    private void displayToMetricConverterOptions() {
        System.out.println("1. Meters to feet");
        System.out.println("2. Centimeters to inches");
        System.out.println("3. Liters to gallons");
        System.out.println("4. Kilograms to pounds");
        System.out.println("5. Celsius to Fahrenheit");
        System.out.println("6. Back to main");
    }

    private void launchToImperialConverter() {
        boolean running = true;
        double convertedValue;
        double enteredValue;
        String sourceUnit;
        String targetUnit;
        while (running) {
            displayToImperialConverterOptions();
            String option = scanner.next();
            switch (option) {
                case "1":
                    sourceUnit = Units.FEET.getUnitName();
                    targetUnit = Units.METERS.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = imperialToMetricConverter.convertFeetToMeters(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "2":
                    sourceUnit = Units.INCHES.getUnitName();
                    targetUnit = Units.CENTIMETERS.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = imperialToMetricConverter.convertInchesToCentimeters(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "3":
                    sourceUnit = Units.GALLONS.getUnitName();
                    targetUnit = Units.LITERS.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = imperialToMetricConverter.convertGallonsToLiters(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "4":
                    sourceUnit = Units.POUNDS.getUnitName();
                    targetUnit = Units.KILOGRAMS.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = imperialToMetricConverter.convertPoundsToKilograms(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "5":
                    sourceUnit = Units.DEGREES_FAHRENHEIT.getUnitName();
                    targetUnit = Units.DEGREES_CELSIUS.getUnitName();
                    enteredValue = getValueToConvert(sourceUnit);
                    convertedValue = imperialToMetricConverter.convertFahrenheitToCelsius(enteredValue);
                    displayConversionResult(enteredValue, convertedValue, sourceUnit, targetUnit);
                    break;
                case "6":
                    running = false;
                    break;
                default:
                    System.out.println("Wrong option, try again!");
                    break;
            }
        }
    }

    private void displayToImperialConverterOptions() {
        System.out.println("1. Feet to meters");
        System.out.println("2. Inches to centimeters");
        System.out.println("3. Gallons to liters");
        System.out.println("4. Pounds to kilograms");
        System.out.println("5. Fahrenheit to Celsius");
        System.out.println("6. Back to main");
    }

    private double getValueToConvert(String sourceUnits) {
        displayConvertPrompt(sourceUnits);
        return scanner.nextDouble();
    }

    private void displayConvertPrompt(String unitName) {
        System.out.println("Enter value to convert (in " + unitName + "): ");
    }

    private void displayConversionResult(double enteredValue, double conversionResult, String sourceUnits, String targetUnits) {
        System.out.println("Conversion result: " + enteredValue + " " + sourceUnits + " is equal to " + conversionResult + " "
                + targetUnits);

    }

}
