package com.mszewczyk.converter_app;

public class MetricToImperialConverter {

    private String valueIsLessThanZeroException = "Value entered cannot be less than zero!";

    public double convertMetersToFeet(double metersEntered) {
        final double factor = 3.28;
        if(metersEntered < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * metersEntered;
    }

    public double convertCentimetersToInches(double centimeters) {
        final double factor = 0.39;
        if(centimeters < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * centimeters;
    }

    public double convertLitersToGallons(double liters) {
        final double factor = 0.22;
        if(liters < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * liters;
    }

    public double convertKilogramsToPounds(double kilograms) {
        final double factor = 2.20;
        if(kilograms < 0){
            throw new IllegalArgumentException(valueIsLessThanZeroException);
        }
        return factor * kilograms;
    }

    public double convertCelsiusToFahrenheit(double celsiusDegrees) {
        final double factor = 1.8;
        return (factor * celsiusDegrees) + 32;
    }
}
