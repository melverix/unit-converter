package com.mszewczyk.converter_app;

public enum Units {

    //length
    CENTIMETERS("centimeters"),
    INCHES("inches"),
    FEET("feet"),
    METERS("meters"),

    //volume
    LITERS("litres"),
    GALLONS("gallons"),

    //weight
    KILOGRAMS("kilograms"),
    POUNDS("pounds"),

    //temperature
    DEGREES_CELSIUS("degrees Celsius"),
    DEGREES_FAHRENHEIT("degrees Fahrenheit");


    private final String unitName;

    Units(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
}

